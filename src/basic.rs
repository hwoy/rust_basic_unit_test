#![allow(dead_code)]

#[cfg(test)]
pub mod tests {
    use std::mem::size_of;

    #[test]
    pub fn test_sizeoftype_equal_sucess() {
        assert_eq!(size_of::<u8>(), 1);
        assert_eq!(size_of::<i8>(), 1);

        assert_eq!(size_of::<i16>(), 2);
        assert_eq!(size_of::<u16>(), 2);

        assert_eq!(size_of::<i32>(), 4);
        assert_eq!(size_of::<u32>(), 4);

        assert_eq!(size_of::<i64>(), 8);
        assert_eq!(size_of::<u64>(), 8);

        assert_eq!(size_of::<i128>(), 16);
        assert_eq!(size_of::<u128>(), 16);

        assert_eq!(size_of::<f32>(), 4);
        assert_eq!(size_of::<f64>(), 8);
    }

    #[test]
    pub fn test_option_work_success() {
        assert_eq!(1u32.checked_div(0u32), None);
        assert_eq!(10i32.checked_div(3i32), Some(10i32 / 3i32));
        assert_eq!(Some(100).unwrap(), 100);

        #[repr(transparent)]
        struct Sam {
            num: u64,
        }

        assert_eq!(size_of::<Sam>(), size_of::<u64>());

        assert_eq!(size_of::<Option<&()>>(), std::mem::size_of::<&()>());
        assert_eq!(size_of::<Option<fn()>>(), std::mem::size_of::<fn()>());
        assert_eq!(
            size_of::<Option<std::ptr::NonNull<()>>>(),
            std::mem::size_of::<std::ptr::NonNull<()>>()
        );
        assert_eq!(
            size_of::<Option<extern "C" fn()>>(),
            std::mem::size_of::<extern "C" fn()>()
        );

        assert_eq!(
            size_of::<Option<*const i32>>() == std::mem::size_of::<*const i32>(),
            false
        );
    }

    #[test]
    pub fn test_result_work_success() {
        let rok: Result<i32, &str> = Ok(2);
        let rerr: Result<i32, &str> = Err("Error");

        assert_eq!(rok, Ok(2));
        assert_eq!(rerr, Err("Error"));
    }

    #[test]
    pub fn test_integer_work_success() {
        assert_eq!(0 + 0, 0);
        assert_eq!(1 + 0, 1);
        assert_eq!(0 + 1, 1);
        assert_eq!(0 + 1, 1 + 0);

        assert_eq!(0 - 0, 0);
        assert_eq!(1 - 0, 1);
        assert_eq!(0 - 1, -1);

        assert_eq!(1 * 0, 0);
        assert_eq!(0 * 1, 0);
        assert_eq!(1 * 0, 0 * 1);
        assert_eq!(1 * 1, 1);

        assert_eq!(1 / 1, 1);
        assert_eq!(1 / 2, 0);
        assert_eq!(1 / 3, 0);

        assert_eq!(1 % 1, 0);
        assert_eq!(1 % 2, 1);
        assert_eq!(1 % 3, 1);
    }

    #[test]
    pub fn test_float_work_success() {
        assert_eq!(0f32 + 0.0f32, 0f32);
        assert_eq!(1.5f32 + 0.0f32, 1.5f32);
        assert_eq!(0.0f32 + 1.5f32, 1.5f32);
        assert_eq!(0.0f32 + 1.5f32, 1.5f32 + 0.0f32);

        assert_eq!(0.0f64 - 0.0f64, 0f64);
        assert_eq!(1.5f64 - 0.0f64, 1.5f64);
        assert_eq!(0.0f64 - 1.5f64, -1.5f64);

        assert_eq!(1.0f32 * 0.0f32, 0.0f32);
        assert_eq!(0.0f32 * 1.0f32, 0.0f32);
        assert_eq!(1.0f32 * 0.0f32, 0.0f32 * 1.0f32);
        assert_eq!(1.0f32 * 1.0f32, 1.0f32);

        assert_eq!(1.0f64 / 1.0f64, 1.0f64);
        assert_eq!(1.0f64 / 2.0f64, 0.5f64);
    }

    #[test]
    #[allow(useless_ptr_null_checks)]
    pub fn test_reference_work_success() {
        let r = &100;
        assert_eq!((r as *const i32).is_null(), false);
        assert_eq!(r, &100);
        assert_eq!(*r, 100);
    }
    pub fn test_pointer_work_success() {
        let p = &100 as *const i32;
        assert_eq!(p.is_null(), false);
        assert_eq!(unsafe { *p }, 100);
    }
}
